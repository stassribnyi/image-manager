// Image actions
export const FETCH_IMAGES = 'FETCH_IMAGES';
export const UPDATE_IMAGE = 'UPDATE_IMAGE';
export const DELETE_IMAGE = 'DELETE_IMAGE';
export const FETCH_IMAGE = 'FETCH_IMAGE';
export const RESET_IMAGE = 'RESET_IMAGE';
export const EDIT_IMAGE = 'EDIT_IMAGE';
export const ADD_IMAGE = 'ADD_IMAGE';

// Pagination actions
export const SET_PAGE_NUMBER = 'SET_PAGE_NUMBER';
export const SET_ITEMS_COUNT = 'SET_ITEMS_COUNT';
export const SET_PAGE_SIZE = 'SET_PAGE_SIZE';

// Modal actions
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const SHOW_MODAL = 'SHOW_MODAL';

